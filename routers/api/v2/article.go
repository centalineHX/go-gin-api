package v1

import (
	"go-gin-api/pkg/app"
	"go-gin-api/pkg/e"
	"go-gin-api/pkg/setting"
	"go-gin-api/pkg/util"
	"go-gin-api/service/article_service"
	"go-gin-api/service/tag_service"
	"net/http"

	"github.com/astaxie/beego/validation"
	"github.com/gin-gonic/gin"
	"github.com/unknwon/com"
)

// @Summary 获取单个文章信息
// @Tags   Article 文章信息
// @Accept json
// @Produce  json
// @Param token  header  string  true  "Header Token"
// @Param id path string true "文章key"
// @Success 200 {object} models.Article
// @Router /api/v1/article/{id} [get]
func GetArticle(c *gin.Context) {
	appG := &app.Gin{C: c}
	id := c.Param("id")
	valid := validation.Validation{}
	valid.Required(id, "id").Message("ID不能为空")
	if valid.HasErrors() {
		app.MarkErrors(valid.Errors)
		appG.Response(http.StatusBadRequest, e.INVALID_PARAMS, nil)
		return

	}

	articleSevice := &article_service.Article{ID: id}

	exist, err := articleSevice.ExistByID()
	if err != nil {
		appG.Response(http.StatusInternalServerError, e.ERROR_CHECK_EXIST_ARTICLE_FAIL, nil)
		return

	}

	if exist {
		appG.Response(http.StatusOK, e.ERROR_NOT_EXIST_ARTICLE, nil)
		return
	}

	data, err := articleSevice.Get()
	if err != nil {
		appG.Response(http.StatusInternalServerError, e.ERROR_GET_ARTICLE_FAIL, nil)
		return
	}

	appG.Response(http.StatusOK, e.SUCCESS, data)

}

// @Summary 获取多个文章信息
// @Tags   Article 文章信息
// @Accept json
// @Produce  json
// @Param token  header  string  true  "Header Token"
// @Param tag_id query string fasle "文章标签key"
// @Param state query string fasle "状态0，1"
// @Success 200 {object} models.Article
// @Router /api/v1/articles [get]
func GetArticles(c *gin.Context) {
	appG := &app.Gin{C: c}
	tag_id := c.Query("tag_id")

	var state int = -1
	if arg := c.Query("state"); arg != "" {
		state = com.StrTo(arg).MustInt()
	}

	articleSevice := &article_service.Article{
		TagID: tag_id,
		State: state,

		PageNum:  util.GetPage(c),
		PageSize: setting.AppSetting.PageSize,
	}

	articles, err := articleSevice.GetAll()
	if err != nil {
		appG.Response(http.StatusInternalServerError, e.ERROR_GET_ARTICLES_FAIL, nil)
		return
	}

	count, err := articleSevice.Count()
	if err != nil {
		appG.Response(http.StatusInternalServerError, e.ERROR_COUNT_ARTICLE_FAIL, nil)
		return
	}

	appG.Response(http.StatusOK, e.SUCCESS, map[string]interface{}{
		"list":  articles,
		"total": count,
	})

}

type AddArticleForm struct {
	TagID     string `form:"tag_id" valid:"Required"`
	Title     string `form:"title" valid:"Required;ManSize(100)"`
	Desc      string `form:"desc" valid:"Required;ManSize(100)"`
	Content   string `form:"content" valid:"Required;ManSize(100)"`
	CreatedBy string `form:"created_by" valid:"Required;ManSize(100)"`

	State int `form:"State" valid:"Range(0,1)"`
}

// @Summary 新增文章标签
// @Tags   Article 文章信息
// @Accept json
// @Produce  json
// @Param token  header  string  true  "Header Token"
// @Param tag_id query string true "文章标签key"
// @Param title query string true "文章标题"
// @Param desc query string true "文章表述"
// @Param content query string true "文章内容"
// @Param created_by query string true "创建人"
// @Param state query int true "状态=>0，1"
// @Success 200 {object} models.Response
// @Router /api/v1/article [post]
func AddArticle(c *gin.Context) {

	var (
		appG     = app.Gin{C: c}
		formData *AddArticleForm
	)
	httpCode, errCode := app.BindAndValid(c, &formData)

	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, nil)
		return

	}

	articleService := &article_service.Article{
		TagID:     formData.TagID,
		Title:     formData.Title,
		Desc:      formData.Desc,
		Content:   formData.Content,
		CreatedBy: formData.CreatedBy,
		State:     formData.State,
	}

	tagService := &tag_service.Tag{ID: formData.TagID}

	exist, err := tagService.ExistByID()
	if err != nil {
		appG.Response(http.StatusInternalServerError, e.ERROR_GET_TAGS_FAIL, nil)
		return

	}

	if exist {
		appG.Response(http.StatusOK, e.ERROR_EXIST_TAG, nil)
		return
	}

	err = articleService.Add()
	if err != nil {
		appG.Response(http.StatusInternalServerError, e.ERROR_ADD_ARTICLE_FAIL, nil)
	}

	appG.Response(http.StatusOK, e.SUCCESS, nil)

}

type EditArticleForm struct {
	ID         string `form:"id" valid:"Required"`
	TagID      string `form:"tag_id" valid:"Required"`
	Title      string `form:"title" valid:"Required;ManSize(100)"`
	Desc       string `form:"desc" valid:"Required;ManSize(100)"`
	Content    string `form:"content" valid:"Required;ManSize(100)"`
	ModifiedBy string `form:"modified_by" valid:"Required;ManSize(100)"`

	State int `form:"State" valid:"Range(0,1)"`
}

// @Summary 编辑文章标签
// @Tags   Article 文章信息
// @Accept json
// @Produce  json
// @Param token  header  string  true  "Header Token"
// @Param id path string true "被编辑文章key"
// @Param tag_id query string false "文章标签id"
// @Param title query string false "文章标题"
// @Param desc query string false "文章表述"
// @Param content query string false "文章内容"
// @Param modified_by query string true "编辑人"
// @Param state query int false "状态=>0，1"
// @Success 200 {object} models.Response
// @Router /api/v1/article/{id} [put]
func EditArticle(c *gin.Context) {

	var (
		appG     = app.Gin{C: c}
		formData = &EditArticleForm{ID: c.Param("id")}
	)

	httpCode, errCode := app.BindAndValid(c, &formData)

	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, nil)
		return

	}

	articleService := &article_service.Article{
		TagID:      formData.TagID,
		Title:      formData.Title,
		Desc:       formData.Desc,
		Content:    formData.Content,
		ModifiedBy: formData.ModifiedBy,
		State:      formData.State,
	}
	tagService := &tag_service.Tag{ID: formData.TagID}

	exist, err := articleService.ExistByID()
	if err != nil {
		appG.Response(http.StatusInternalServerError, e.ERROR_GET_ARTICLE_FAIL, nil)
		return
	}

	if !exist {
		appG.Response(http.StatusBadRequest, e.ERROR_GET_ARTICLE_FAIL, nil)
		return
	}

	exist, err = tagService.ExistByID()
	if err != nil {
		appG.Response(http.StatusInternalServerError, e.ERROR_GET_TAGS_FAIL, nil)
		return

	}

	if exist {
		appG.Response(http.StatusOK, e.ERROR_EXIST_TAG, nil)
		return
	}

	err = articleService.Edit()
	if err != nil {
		appG.Response(http.StatusInternalServerError, e.ERROR_EDIT_ARTICLE_FAIL, nil)
	}

	appG.Response(http.StatusOK, e.SUCCESS, nil)

}

// @Summary 删除文章
// @Tags   Article 文章信息
// @Accept json
// @Produce  json
// @Param token  header  string  true  "Header Token"
// @Param id path string true "被删除文章key"
// @Success 200 {object} models.Response
// @Router /api/v1/article/{id} [delete]
func DeleteArticle(c *gin.Context) {

	appG := &app.Gin{C: c}
	valid := validation.Validation{}
	ID := c.Param("id")
	valid.Required(ID, "id").Message("ID不能为空")

	if valid.HasErrors() {
		app.MarkErrors(valid.Errors)
		appG.Response(http.StatusBadRequest, e.INVALID_PARAMS, nil)
		return

	}

	tarticleService := &article_service.Article{ID: ID}

	exist, err := tarticleService.ExistByID()
	if err != nil {
		appG.Response(http.StatusInternalServerError, e.ERROR_GET_ARTICLE_FAIL, nil)
		return
	}

	if !exist {
		appG.Response(http.StatusBadRequest, e.ERROR_NOT_EXIST_ARTICLE, nil)
		return
	}

	err = tarticleService.Delete()
	if err != nil {
		appG.Response(http.StatusInternalServerError, e.ERROR_DELETE_ARTICLE_FAIL, nil)
	}

	appG.Response(http.StatusOK, e.SUCCESS, nil)

}
