package jwt

import (
	"go-gin-api/pkg/e"
	"go-gin-api/pkg/logging"
	"go-gin-api/pkg/util"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/unknwon/com"
)

func JWT() gin.HandlerFunc {
	return func(c *gin.Context) {
		var code int
		var data interface{}

		code = e.SUCCESS
		//token := c.Query("token")
		token := com.StrTo(c.GetHeader("token")).String()
		if token == "" {
			code = e.INVALID_PARAMS
		} else {
			//query token 验证
			claims, err := util.ParseToken(token)
			if err != nil {
				//token校验不通过
				code = e.ERROR_AUTH_CHECK_TOKEN_FAIL
			} else if time.Now().Unix() > claims.ExpiresAt {
				//token 过期
				code = e.ERROR_AUTH_CHECK_TOKEN_TIMEOUT
			}
		}

		if code != e.SUCCESS {
			msg := e.GetMsg(code)
			logging.Error(msg)

			c.JSON(http.StatusUnauthorized, gin.H{
				"code": code,
				"msg":  msg,
				"data": data,
			})

			c.Abort()

			return
		}

		c.Next()
	}
}
