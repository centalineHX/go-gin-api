package gredis

import (
	"encoding/json"
	"go-gin-api/pkg/setting"
	"time"

	"github.com/gomodule/redigo/redis"
)

var (
	RedisConn   *redis.Pool
	EXPIRE_TIME int = 36000
)

func Setup() error {

	RedisConn = &redis.Pool{

		MaxIdle:     setting.RedisSetting.MaxIdle,     //最大空闲链接书
		MaxActive:   setting.RedisSetting.MaxActive,   //在给定时间内，允许分配的最大连接数
		IdleTimeout: setting.RedisSetting.IdleTimeout, //在给定时间内将会保持空闲状态，若到达限制时间则关闭链接（为0时，则没有限制）
		Dial: func() (redis.Conn, error) {
			//提供创建和配置应用程序链接的一个函数
			c, err := redis.Dial("tcp", setting.RedisSetting.Host)
			if err != nil {
				return nil, err
			}

			if setting.RedisSetting.Password != "" {
				if _, err := c.Do("AUTH", setting.RedisSetting.Password); err != nil {
					c.Close()
					return nil, err
				}
			}

			return c, err
		},
		TestOnBorrow: func(c redis.Conn, t time.Time) error {
			//可选的应用程序检查健康的功能
			_, err := c.Do("PING")
			return err
		},
	}

	return nil

}

/*
文件内包含 Set、Exists、Get、Delete、LikeDeletes 用于支撑目前的业务逻辑，而在里面涉及到了如方法：

（1）RedisConn.Get()：在连接池中获取一个活跃连接

（2）conn.Do(commandName string, args ...interface{})：向 Redis 服务器发送命令并返回收到的答复

（3）redis.Bool(reply interface{}, err error)：将命令返回转为布尔值

（4）redis.Bytes(reply interface{}, err error)：将命令返回转为 Bytes

（5）redis.Strings(reply interface{}, err error)：将命令返回转为 []string*/

func Set(key string, data interface{}) error {
	conn := RedisConn.Get()
	defer conn.Close()

	value, err := json.Marshal(data)
	if err != nil {
		return err
	}

	_, err = conn.Do("SET", key, value)
	if err != nil {
		return err
	}

	_, err = conn.Do("EXPIRE", key, EXPIRE_TIME)
	if err != nil {
		return err
	}

	return nil

}

func Exists(key string) bool {
	conn := RedisConn.Get()
	defer conn.Close()

	exists, err := redis.Bool(conn.Do("EXISTS", key))
	if err != nil {
		return false
	}

	return exists
}

func Get(key string) ([]byte, error) {
	conn := RedisConn.Get()
	defer conn.Close()

	reply, err := redis.Bytes(conn.Do("GET", key))

	if err != nil {
		return nil, err
	}

	return reply, nil
}

func Delete(key string) (bool, error) {
	conn := RedisConn.Get()
	defer conn.Close()

	return redis.Bool(conn.Do("DEL", key))

}

func LikeDeletes(key string) error {
	conn := RedisConn.Get()
	defer conn.Close()

	keys, err := redis.Strings(conn.Do("KEYS", "*"+key+"*"))
	if err != nil {
		return err
	}

	for _, key := range keys {
		if _, err := Delete(key); err != nil {
			return err
		}
	}

	return nil

}
