package main

import (
	"fmt"
	"go-gin-api/models"
	"go-gin-api/pkg/setting"
	"go-gin-api/routers"
	"net/http"
)

func main() {

	setting.Setup()
	models.Setup()
	setting.Setup()

	router := routers.InitRouter()

	s := &http.Server{
		Addr:           fmt.Sprintf(":%d", setting.ServerSeting.HttpPort), //监听TCP端口：8100
		Handler:        router,                                            //http句柄，ServerHttp用于处理响应的http请求
		ReadTimeout:    setting.ServerSeting.ReadTimeout,                  //允许读取的最大时间
		WriteTimeout:   setting.ServerSeting.WriteTimeout,                 //允许写的最大时间
		MaxHeaderBytes: 1 << 20,                                           //请求头的最大字节 1*2 20 次方

		//IdleTimeout time.Duration  等待的最大时间
		//ConnState func(net.Conn, ConnState)  指定一个可选的回调函数，当客户端连接发生变化时调用
		//ErrorLog *log.Logger  指定一个可选的日志记录器，用于接收程序的意外行为和底层系统错误；如果未设置或为nil则默认以日志包的标准日志记录器完成（也就是在控制台输出）
	}

	s.ListenAndServe()

}
