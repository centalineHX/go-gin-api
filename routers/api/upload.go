package api

import (
	"go-gin-api/pkg/app"
	"go-gin-api/pkg/e"
	"go-gin-api/pkg/logging"
	"go-gin-api/pkg/upload"
	"net/http"

	"github.com/gin-gonic/gin"
)

// @Summary  图片上传
// @Tags   File文件上传
// @Accept json
// @Produce  json
// @Param   image formData file true  "图片附件"
// @Success 200 {object} models.Response
// @Router /upload/image [post]
func UploadImage(c *gin.Context) {

	appG := &app.Gin{C: c}
	code := e.SUCCESS
	data := make(map[string]interface{})
	// 入参 -> 检查 -》 保存 的应用流程
	_, image, err := c.Request.FormFile("image")
	if err != nil {
		logging.Warn(err)
		code = e.ERROR

		appG.Response(http.StatusBadRequest, code, data)

	}

	if image == nil {
		code = e.INVALID_PARAMS
	} else {
		imageName := upload.GetImageName(image.Filename)
		fullPath := upload.GetImageFullPath()
		savePath := upload.GetImagePath()

		src := fullPath + imageName

		if !upload.CheckImageExt(imageName) {
			code = e.ERROR_UPLOAD_CHECK_IMAGE_FORMAT
		} else {
			err := upload.CheckImage(fullPath)
			if err != nil {
				logging.Warn(err)
				code = e.ERROR_UPLOAD_CHECK_IMAGE_FAIL
			} else if err := c.SaveUploadedFile(image, src); err != nil {
				logging.Warn(err)
				code = e.ERROR_UPLOAD_SAVE_IMAGE_FAIL

			} else {
				data["image_url"] = upload.GetImageFullUrl(imageName)
				data["image_save_url"] = savePath + imageName
			}

		}
	}
	appG.Response(http.StatusBadRequest, code, data)

	// c.JSON(http.StatusOK, models.Response{
	// 	Code: code,
	// 	Msg:  e.GetMsg(code),
	// 	Data: data,
	// })

}
