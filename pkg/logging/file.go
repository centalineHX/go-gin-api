package logging

import (
	"fmt"
	"go-gin-api/pkg/file"
	"go-gin-api/pkg/setting"
	"log"
	"os"
	"strings"
	"time"
)

func getLogFilePath() string {
	var path strings.Builder
	path.WriteString(setting.AppSetting.RuntimeRootPath)
	path.WriteString(setting.AppSetting.LogSavePath)
	return path.String()
}

func getLogFileName() string {
	prefixPath := getLogFilePath()
	suffixPath := fmt.Sprintf("%s_%s.%s",
		setting.AppSetting.LogSaveName,
		time.Now().Format(setting.AppSetting.TimeFormat),
		setting.AppSetting.LogFileExt)

	return fmt.Sprintf("%s%s", prefixPath, suffixPath)
}

func openLogFile(fileName, filePath string) (*os.File, error) {
	//os.Stat：返回文件信息结构描述文件。如果出现错误，会返回*PathError
	dir, err := os.Getwd()
	if err != nil {
		return nil, fmt.Errorf("os.Getwd err: %v", err)

	}
	src := dir + "/" + filePath
	perm := file.CheckPermission(src)
	if perm {
		return nil, fmt.Errorf("file.CheckPermission Permission denied src: %s", src)
	}

	err = file.IsNotExistMkDir(src)
	if err != nil {
		return nil, fmt.Errorf("file.IsNotExistMkDir src: %s, err: %v", src, err)
	}

	//os.OpenFile：调用文件，支持传入文件名称、指定的模式调用文件、文件权限，返回的文件的方法可以用于 I/O。如果出现错误，则为*PathError。
	/*
			const (
		    // Exactly one of O_RDONLY, O_WRONLY, or O_RDWR must be specified.
		    O_RDONLY int = syscall.O_RDONLY // 以只读模式打开文件
		    O_WRONLY int = syscall.O_WRONLY // 以只写模式打开文件
		    O_RDWR   int = syscall.O_RDWR   // 以读写模式打开文件
		    // The remaining values may be or'ed in to control behavior.
		    O_APPEND int = syscall.O_APPEND // 在写入时将数据追加到文件中
		    O_CREATE int = syscall.O_CREAT  // 如果不存在，则创建一个新文件
		    O_EXCL   int = syscall.O_EXCL   // 使用O_CREATE时，文件必须不存在
		    O_SYNC   int = syscall.O_SYNC   // 同步IO
		    O_TRUNC  int = syscall.O_TRUNC  // 如果可以，打开时
		)
	*/
	f, err := file.Open(src+fileName, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		log.Fatalf("Fail to OpenFile:%v", err)
	}

	return f, nil
}

// func mkDir() {
// 	//Getwd 返回对应于当前目录的根路径名。如果当前目录可以通过多个路径（由于符号链接）到达，Getwd 可能会返回其中任何一个。
// 	dir, _ := os.Getwd()
// 	//os.MkdirAll：创建对应的目录以及所需的子目录，若成功则返回nil，否则返回error
// 	err := os.MkdirAll(dir+"/"+getLogFilePath(), os.ModePerm) //ModePerm FileMode = 0777
// 	if err != nil {
// 		panic(err)
// 	}
// }
