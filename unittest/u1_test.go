package unittest

import (
	"fmt"
	"sync"
	"sync/atomic"
	"testing"
	"time"
)

type Cat struct {
	name           string // 名字。
	scientificName string // 学名。
	category       string // 动物学基本分类。
}

func New(name, scientificName, category string) Cat {
	return Cat{
		name:           name,
		scientificName: scientificName,
		category:       category,
	}
}

func (cat *Cat) SetName(name string) {
	cat.name = name
}

func (cat Cat) SetNameOfCopy(name string) {
	cat.name = name
}

func (cat Cat) Name() string {
	return cat.name
}

func (cat Cat) ScientificName() string {
	return cat.scientificName
}

func (cat Cat) Category() string {
	return cat.category
}

func (cat Cat) String() string {
	return fmt.Sprintf("%s (category: %s, name: %q)",
		cat.scientificName, cat.category, cat.name)
}

func TestValueAndPoints(t *testing.T) {
	cat := New("little pig", "American Shorthair", "cat")
	cat.SetName("monster") // (&cat).SetName("monster")
	fmt.Printf("The cat: %s\n", cat.name)

	cat.SetNameOfCopy("little pig")
	fmt.Printf("The cat: %s\n", cat.name)

	type Pet interface {
		SetName(name string)
		Name() string
		Category() string
		ScientificName() string
	}

	_, ok := interface{}(cat).(Pet)
	fmt.Printf("Cat implements interface Pet: %v\n", ok)
	_, ok = interface{}(&cat).(Pet)
	fmt.Printf("*Cat implements interface Pet: %v\n", ok)
}

func TestGoroutine(t *testing.T) {
	var wg sync.WaitGroup

	for i := 0; i < 10; i++ {
		wg.Add(1)
		go func(i int) {
			fmt.Println(i)
			wg.Done()
		}(i)
		wg.Wait()
	}

}

func TestGoroutine2(t *testing.T) {
	var mu sync.Mutex

	for i := 0; i < 10; i++ {

		go func(i int) {
			defer func(i int) {

				fmt.Println(i)
				mu.Unlock()
			}(i)

			mu.Lock()
		}(i)

	}

}

func TestGoRoutine3(t *testing.T) {
	var count uint32
	trigger := func(i uint32, fn func()) {
		for {
			if n := atomic.LoadUint32(&count); n == i {
				fn()
				atomic.AddUint32(&count, 1)
				break
			}
			//time.Sleep(time.Nanosecond)
		}
	}
	for i := uint32(0); i < 10; i++ {
		go func(i uint32) {
			fn := func() {
				fmt.Println(i)
			}
			trigger(i, fn)
		}(i)
	}
	trigger(10, func() {})

}

func TestArray(t *testing.T) {
	numbers2 := [...]int{1, 2, 3, 4, 5, 6}
	maxIndex2 := len(numbers2) - 1
	for i, e := range numbers2 {
		if i == maxIndex2 {
			numbers2[0] += e
		} else {
			numbers2[i+1] += e
		}
	}
	fmt.Println(numbers2)
}

func TestSlice(t *testing.T) {
	numbers2 := []int{1, 2, 3, 4, 5, 6}
	maxIndex2 := len(numbers2) - 1
	fmt.Println(maxIndex2)
	//切片 引用类型处理，range表达式求值结果是对原数据类型的直接引用，
	//所以后面计算结果所改变的 切面值也是直接影响原基础数据
	for i, e := range numbers2 {
		if i == maxIndex2 {
			numbers2[0] += e
		} else {
			numbers2[i+1] += e
		}
	}
	fmt.Println(numbers2)
}

func TestSyncCond(t *testing.T) {

	mutex := sync.Mutex{}
	var cond = sync.NewCond(&mutex)
	mail := 1
	go func() {
		for count := 0; count <= 15; count++ {
			time.Sleep(time.Second)
			mail = count
			cond.Broadcast()
		}
	}()
	// worker1
	go func() {
		for mail != 5 {
			// 触发的条件，如果不等于5，就会进入cond.Wait()等待，
			//此时cond.Broadcast()通知进来的时候，wait阻塞解除，
			//进入下一个循环，此时发现mail != 5，跳出循环，开始工作。
			cond.L.Lock()
			cond.Wait()
			cond.L.Unlock()
		}

		fmt.Printf("worker1 started to work mail=%d", mail)
		fmt.Println()
		time.Sleep(3 * time.Second)
		fmt.Println("worker1 work end")
	}()
	// worker2
	go func() {
		for mail != 10 {
			cond.L.Lock()
			cond.Wait()
			cond.L.Unlock()
		}

		fmt.Printf("worker2 started to work mail=%d", mail)
		fmt.Println()
		time.Sleep(3 * time.Second)
		fmt.Println("worker2 work end")
	}()
	// worker3
	go func() {
		for mail != 10 {
			cond.L.Lock()
			cond.Wait()
			cond.L.Unlock()
		}

		fmt.Printf("worker3 started to work mail=%d", mail)
		fmt.Println()
		time.Sleep(3 * time.Second)
		fmt.Println("worker3 work end")
	}()
	select {}
}
