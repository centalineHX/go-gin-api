package models

import (
	"fmt"
	"go-gin-api/pkg/setting"
	"log"
	"time"

	"github.com/google/uuid"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	//"go-gin-api/pkg/setting"
)

var db *gorm.DB

type Model struct {
	ID         string `gorm:"primary_key" json:"id"` //主键key
	CreatedOn  int    `json:"created_on"`            //创建时间戳
	ModifiedOn int    `json:"modified_on"`           //编辑时间戳
	DeletedTag int    `json:"deleted_tag"`           //删除标识
}

func Setup() {

	var (
		err                                               error
		dbType, dbName, user, password, host, tablePrefix string
	)

	dbType = setting.DatebaseSetting.Type
	dbName = setting.DatebaseSetting.Name
	user = setting.DatebaseSetting.User
	password = setting.DatebaseSetting.Password
	host = setting.DatebaseSetting.Host
	tablePrefix = setting.DatebaseSetting.TablePrefix

	db, err = gorm.Open(dbType, fmt.Sprintf("%s:%s@tcp(%s)/%s?charset=utf8&parseTime=True&loc=Local",
		user,
		password,
		host,
		dbName))

	if err != nil {
		log.Println(err)
	}

	gorm.DefaultTableNameHandler = func(db *gorm.DB, defaultTableName string) string {
		return tablePrefix + defaultTableName
	}

	db.SingularTable(true)
	db.LogMode(true)
	db.DB().SetMaxIdleConns(10)
	db.DB().SetMaxOpenConns(100)

	db.Callback().Create().Replace("gorm:update_time_stamp", updateTimeStampForCreateCallback)
	db.Callback().Update().Replace("gorm:update_time_stamp", updateTimeStampForUpdateCallback)
	db.Callback().Delete().Replace("gorm:delete", deleteCallback)
	//db.Callback().Query().Replace("gorm:preload", queryCallback)
	//db.Callback().Query().After("gorm:query").Register("my_plugin:after_query", queryCallback)
}

func CloseDB() {
	defer db.Close()

}

func AddDeletedTagField(m interface{}) (map[string]interface{}, bool) {

	if filter, ok := m.(map[string]interface{}); ok {
		filter["deleted_tag"] = 0
		return filter, true
	} else {

		return nil, false
	}

}

// func updateTimeStampForQueryCallback(scope *gorm.Scope) {
// 	if _, ok := scope.FieldByName("DeletedTag"); ok {

// 		scope.SQLDB().Query(scope.SQL)
// 	}
// }

// func queryCallback(scope *gorm.Scope) {
// 	if !scope.HasError() {

// 		fmt.Print(scope.SQL)
// 		fmt.Println()

// 	}
// }

func updateTimeStampForCreateCallback(scope *gorm.Scope) {
	if !scope.HasError() {
		nowTime := time.Now().Unix() //时间戳
		newid := uuid.New().String()

		if idField, ok := scope.FieldByName("ID"); ok {

			//isBlank 判断值是否为空
			if idField.IsBlank {
				idField.Set(newid)
			}
		}

		if createTimeField, ok := scope.FieldByName("CreatedOn"); ok {

			//isBlank 判断值是否为空
			if createTimeField.IsBlank {
				createTimeField.Set(nowTime)
			}
		}

		if modifyTimeField, ok := scope.FieldByName("ModifiedOn"); ok {
			if modifyTimeField.IsBlank {
				modifyTimeField.Set(nowTime)
			}

		}

	}

}

//scope.Get(...) 根据入参获取设置了字面值的参数，例如本文中是 gorm:update_column ，它会去查找含这个字面值的字段属性
//cope.SetColumn(...) 假设没有指定 update_column 的字段，我们默认在更新回调设置 ModifiedOn 的值
func updateTimeStampForUpdateCallback(scope *gorm.Scope) {
	if _, ok := scope.Get("gorm:update_column"); !ok {
		scope.SetColumn("ModifiedOn", time.Now().Unix())
	}

}

func deleteCallback(scope *gorm.Scope) {
	if !scope.HasError() {
		var extraOption string
		if str, ok := scope.Get("gorm:delete_option"); ok {
			extraOption = fmt.Sprint(str)
		}

		deletedTagField, hasDeletedTagField := scope.FieldByName("DeletedTag")
		modifiedField, _ := scope.FieldByName("ModifiedOn")
		if !scope.Search.Unscoped && hasDeletedTagField {
			scope.Raw(fmt.Sprintf(
				"UPDATE %v SET %v=%v,%v=%v %v%v",
				scope.QuotedTableName(),
				scope.Quote(deletedTagField.DBName),
				scope.AddToVars(1),
				scope.Quote(modifiedField.DBName),
				scope.AddToVars(time.Now().Unix()),
				addExtraSpaceIfExist(scope.CombinedConditionSql()),
				addExtraSpaceIfExist(extraOption),
			)).Exec()
		} else {
			scope.Raw(fmt.Sprintf(
				"DELETE FROM %v%v%v",
				scope.QuotedTableName(),
				addExtraSpaceIfExist(scope.CombinedConditionSql()),
				addExtraSpaceIfExist(extraOption),
			)).Exec()
		}
	}
}

func addExtraSpaceIfExist(str string) string {
	if str != "" {
		return " " + str
	}
	return ""
}
