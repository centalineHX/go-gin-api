package upload

import (
	"fmt"
	"go-gin-api/pkg/file"
	"go-gin-api/pkg/logging"
	"go-gin-api/pkg/setting"
	"go-gin-api/pkg/util"
	"log"
	"mime/multipart"
	"os"
	"path"
	"strings"
)

//获取图片路径
func GetImagePath() string {
	return setting.AppSetting.ImageSavePath
}

//获取图片访问路径
func GetImageFullUrl(name string) string {
	return setting.AppSetting.ImagePrefixUrl + "/" + GetImagePath() + name
}

//获取图片名称
func GetImageName(name string) string {
	ext := path.Ext(name)
	fileName := strings.TrimSuffix(name, ext)
	fileName = util.EncodeMD5(fileName)

	return fileName + ext

}

//获取图片完成路径
func GetImageFullPath() string {

	return setting.AppSetting.RuntimeRootPath + GetImagePath()
}

//检查图片后缀
func CheckImageExt(filename string) bool {
	ext := file.GetExt(filename)
	var tag bool = false
	for _, allowExt := range setting.AppSetting.ImageAllowExts {
		if strings.EqualFold(strings.ToUpper(allowExt), strings.ToUpper(ext)) {
			tag = true
			break

		}

	}
	return tag
}

//检查图片大小
func CheckImageSize(f multipart.File) bool {
	size, err := file.GetSize(f)
	if err != nil {
		log.Panicln(err)
		logging.Warn(err)
		return false

	}

	return size <= setting.AppSetting.ImageMaxSize
}

//检查图片
func CheckImage(src string) error {
	dir, err := os.Getwd()
	if err != nil {
		return fmt.Errorf("os.Getwd err: %v", err)
	}
	err = file.IsNotExistMkDir(dir + "/" + src)
	if err != nil {
		return fmt.Errorf("file.IsNotExistMkDir err: %v", err)
	}

	perm := file.CheckPermission(src)
	if perm {
		return fmt.Errorf("file.CheckPermission Permission denied src: %s", src)
	}

	return nil

}
