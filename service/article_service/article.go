package article_service

import (
	"encoding/json"
	"go-gin-api/models"
	"go-gin-api/pkg/gredis"
	"go-gin-api/pkg/logging"
	"go-gin-api/service/cache_service"
)

type Article struct {
	ID    string
	TagID string

	Title      string
	Desc       string
	Content    string
	CreatedBy  string
	ModifiedBy string
	State      int

	PageNum  int
	PageSize int
}

func (a *Article) ExistByID() (bool, error) {
	return models.ExistArticleByID(a.ID)
}

func (a *Article) Count() (int, error) {
	return models.GetArticleTotal(a.getMaps())
}

func (a *Article) Add() error {

	article := map[string]interface{}{
		"tag_id":     a.TagID,
		"title":      a.Title,
		"desc":       a.Desc,
		"content":    a.Content,
		"created_by": a.CreatedBy,
		//"cover_image_url": a.CoverImageUrl,
		"state": a.State,
	}

	if err := models.AddArticle(article); err != nil {
		return err
	}

	return nil
}

func (a *Article) Edit() error {
	return models.EditArticle(a.ID, map[string]interface{}{
		"tag_id":  a.TagID,
		"title":   a.Title,
		"desc":    a.Desc,
		"content": a.Content,
		//"cover_image_url": a.CoverImageUrl,
		"state":       a.State,
		"modified_by": a.ModifiedBy,
	})
}

func (a *Article) Delete() error {
	return models.DeleteArticle(a.ID)
}

func (a *Article) Get() (*models.Article, error) {
	var cacheArticle *models.Article

	cache := cache_service.Article{ID: a.ID}
	key := cache.GetArticleKey()
	if gredis.Exists(key) {
		data, err := gredis.Get(key)
		if err != nil {
			logging.Info(err)
		} else {
			json.Unmarshal(data, &cacheArticle)
			return cacheArticle, nil
		}

	}

	article, err := models.GetArticle(a.ID)
	if err != nil {
		return nil, err
	}

	gredis.Set(key, article)
	return article, nil
}

func (a *Article) GetAll() ([]*models.Article, error) {
	var (
		cacheArticles []*models.Article
	)

	cache := cache_service.Article{
		TagID: a.TagID,
		State: a.State,

		PageNum:  a.PageNum,
		PageSize: a.PageSize,
	}

	key := cache.GetArticlesKey()
	if gredis.Exists(key) {

		data, err := gredis.Get(key)
		if err != nil {
			return nil, err
		}

		json.Unmarshal(data, &cacheArticles)

		return cacheArticles, nil

	}

	articles, err := models.GetArticles(a.PageNum, a.PageSize, a.getMaps())
	if err != nil {
		return nil, err
	}
	gredis.Set(key, articles)
	return articles, nil

}

func (t *Article) getMaps() map[string]interface{} {
	maps := make(map[string]interface{})
	maps["deleted_tag"] = 0

	if t.TagID != "" {
		maps["tag_id"] = t.TagID
	}
	if t.State >= 0 {
		maps["state"] = t.State
	}

	return maps
}
