package models

import "github.com/jinzhu/gorm"

type Article struct {
	Model

	TagID string `json:"tag_id" gorm:"index" ` //用于声明这个字段为索引
	Tag   Tag    `json:"tag"`                  //组合，嵌套Tag truct ，进行外键表关联

	Title      string `json:"title"`       //文章标题
	Desc       string `json:"desc"`        //文章描述
	Content    string `json:"content"`     //文章内容
	CreatedBy  string `json:"create_by"`   //创建人
	ModifiedBy string `json:"modified_by"` //编辑人
	State      int    `json:"state"`       //状态
}

func ExistArticleByID(id string) (bool, error) {
	var article Article

	err := db.Select("id").Where("id=?", id).First(&article).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		return false, err

	}
	return article.ID != "", nil

}

func GetArticleTotal(maps interface{}) (int, error) {
	var count int
	err := db.Model(&Article{}).Where(maps).Count(&count).Error
	if err != nil {
		return 0, err
	}
	return count, nil

}

func GetArticles(pageNum int, pageSize int, maps interface{}) ([]*Article, error) {
	var (
		article []*Article
		err     error
	)
	if pageNum > 0 && pageSize > 0 {
		err = db.Preload("Tag").Where(maps).Offset(pageNum).Limit(pageSize).Find(&article).Error
	} else {
		err = db.Preload("Tag").Where(maps).Find(&article).Error
	}

	if err != nil && err != gorm.ErrRecordNotFound {
		return nil, err
	}
	return article, nil
}

func GetArticle(id string) (*Article, error) {
	var article Article
	err := db.Where("id=? AND deleted_tag=?", id, 0).First(&article).Error
	if err != nil && err != gorm.ErrRecordNotFound {

		return nil, err

	}

	//表关联，通过获取tag 所属的article集合，并写入到article的tag实体中
	err = db.Model(&article).Related(&article.Tag).Error
	if err != nil && err != gorm.ErrRecordNotFound {

		return nil, err

	}

	return &article, nil

}

func AddArticle(data map[string]interface{}) error {

	article := Article{
		TagID:     data["tag_id"].(string),
		Title:     data["title"].(string),
		Desc:      data["desc"].(string),
		Content:   data["content"].(string),
		CreatedBy: data["created_by"].(string),
		State:     data["state"].(int),
	}

	if err := db.Create(&article).Error; err != nil {
		return err
	}

	return nil

}

func EditArticle(id string, data map[string]interface{}) error {

	if err := db.Model(&Article{}).Where("id=?", id).Updates(data).Error; err != nil {
		return err
	}
	return nil

}

func DeleteArticle(id string) error {
	if err := db.Where("id=?", id).Delete(Article{}).Error; err != nil {
		return err
	}
	return nil
}

func CleanAllArticle() bool {
	db.Unscoped().Where("deleted_on != ? ", 0).Delete(&Article{})

	return true
}

// func (article *Article) BeforeCreate(scope *gorm.Scope) error {

// 	//time.Now().Unix() 时间戳
// 	scope.SetColumn("CreatedOn", time.Now().Unix())
// 	fmt.Printf("Article BeforeCreate gorm Callbacks")
// 	return nil
// }

// func (article *Article) BeforeUpdate(scope *gorm.Scope) error {
// 	scope.SetColumn("ModifiedOn", time.Now().Unix())
// 	fmt.Printf("Article BeforeUpdate gorm Callbacks")
// 	return nil
// }
