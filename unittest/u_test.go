package unittest

import (
	"fmt"
	"testing"
	"time"
)

func TestTime(t *testing.T) {
	time_s := 120 * time.Hour
	fmt.Print(time_s)
	fmt.Println()
	fmt.Printf("%s", time_s)
	fmt.Println()

}

type Model struct {
	name string
}

func TestModel(t *testing.T) {

	a := &Model{
		name: "123",
	}

	fmt.Printf("model value:%v ", a.name)
	fmt.Println()

	// fmt.Printf("model value:%v ", *&a.name)
	// fmt.Println()

	// fmt.Printf("model value:%v ", a)
	// fmt.Println()
	// fmt.Printf("model value:%v ", b)
	// fmt.Println()
	// m.returnName()
	// fmt.Println()
	// fmt.Printf("model value:%p ", m)
	// fmt.Println()

}

func (m *Model) returnName() {
	m.name = "456"
	fmt.Printf("model value:%p  ", m)

	//fmt.Printf("model 指针:%p", m)

}
