package models

import (
	"github.com/jinzhu/gorm"
)

type Tag struct {
	Model

	Name       string `json:"name" `             //标签名称
	CreatedBy  string `json:"create_by" `        //创建人
	ModifiedBy string `json:"modified_by" `      //编辑人
	State      int    `json:"state" example:"0"` //标签状态
}

func ExistTagByName(name string) (bool, error) {
	var tag Tag
	err := db.Select("id").Where("name=?", name).First(&tag).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		return false, err
	}
	return tag.ID != "", nil
}

func ExistTagByID(id string) (bool, error) {
	var tag Tag
	err := db.Select("id").Where("id=?", id).First(&tag).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		return false, err
	}
	return tag.ID != "", nil
}

func GetTagTotal(maps interface{}) (int, error) {
	var count int

	if err := db.Model(&Tag{}).Where(maps).Count(&count).Error; err != nil {
		return 0, err
	}
	return count, nil
}

//这里return 没有返回值，是因为在函数方法的末尾，已经显示的声明了返回的值，非类型，所以这个变量在函数体内也是可以使用的
//因为一开始就被声明了

//db是的引用，是因为tag.go和 models.go 都是属于一个packgae models,所以可以直接使用被声明好的 db *gorm.DB
func GetTags(pageNum int, pageSize int, maps interface{}) ([]Tag, error) {

	var (
		tags []Tag
		err  error
	)

	if pageSize > 0 && pageNum > 0 {
		err = db.Where(maps).Offset(pageNum).Limit(pageSize).Find(&tags).Error

	} else {
		err = db.Where(maps).Find(&tags).Error
	}

	if err != nil && err != gorm.ErrRecordNotFound {
		return nil, err
	}

	return tags, nil
}

func AddTag(name string, state int, createdBy string) error {

	tag := Tag{
		Name:      name,
		State:     state,
		CreatedBy: createdBy,
	}

	if err := db.Create(&tag).Error; err != nil {
		return err
	}

	return nil

}

func DeleteTag(id string) error {
	if err := db.Where("id=?", id).Delete(&Tag{}).Error; err != nil {
		return err
	}
	return nil
}

func EditTag(id string, data interface{}) error {
	if err := db.Model(&Tag{}).Where("id=?", id).Update(data).Error; err != nil {
		return err
	}
	return nil

}

func CleanAllTag() bool {
	db.Unscoped().Where("deleted_tag != ? ", 0).Delete(&Tag{})

	return true
}

//涉及知识点：gorm Callbacks 可以将回调方法定义为模型结构的指针，在创建、更新、查询、删除时将被调用，如果任何回调返回错误，gorm 将停止未来操作并回滚所有更改。
// gorm所支持的回调方法：

// 创建：BeforeSave、BeforeCreate、AfterCreate、AfterSave
// 更新：BeforeSave、BeforeUpdate、AfterUpdate、AfterSave
// 删除：BeforeDelete、AfterDelete
// 查询：AfterFind
// func (tag *Tag) BeforeCreate(scope *gorm.Scope) error {
// 	scope.SetColumn("CreatedOn", time.Now().Unix())
// 	fmt.Printf("Tag BeforeCreate gorm Callbacks")

// 	return nil
// }

// func (tag *Tag) BeforeUpdate(scope *gorm.Scope) error {
// 	scope.SetColumn("ModifiedOn", time.Now().Unix())
// 	fmt.Printf("Tag BeforeUpdate gorm Callbacks")

// 	return nil
// }
