package v1

import (
	"go-gin-api/pkg/app"
	"go-gin-api/pkg/e"
	"go-gin-api/pkg/setting"
	"go-gin-api/pkg/util"
	"go-gin-api/service/tag_service"
	"net/http"

	"github.com/astaxie/beego/validation"
	"github.com/gin-gonic/gin"
	"github.com/unknwon/com"
)

// @Summary 获取多个文章标签
// @Tags   Tags 标签信息
// @Accept json
// @Produce  json
// @Param token  header  string  true  "Header Token"
// @Param name query string false "标签名称"
// @Param state query int false "状态=>0，1"
// @Success 200 {object} models.Tag
// @Router /api/v1/tags [get]
func GetTags(c *gin.Context) {
	appG := &app.Gin{C: c}
	name := c.Query("name")

	var state int = -1
	if arg := c.Query("state"); arg != "" {
		state = com.StrTo(arg).MustInt()
	}

	tagSevice := &tag_service.Tag{
		Name:     name,
		State:    state,
		PageNum:  util.GetPage(c),
		PageSize: setting.AppSetting.PageSize,
	}

	tags, err := tagSevice.GetAll()
	if err != nil {
		appG.Response(http.StatusInternalServerError, e.ERROR_GET_TAGS_FAIL, nil)
		return
	}

	count, err := tagSevice.Count()
	if err != nil {
		appG.Response(http.StatusInternalServerError, e.ERROR_COUNT_TAG_FAIL, nil)
		return
	}

	appG.Response(http.StatusOK, e.SUCCESS, map[string]interface{}{
		"list":  tags,
		"total": count,
	})

}

type AddTagForm struct {
	Name      string `form:"name" valid:"Required;ManSize(100)"`
	CreatedBy string `form:"created_by" valid:"Required;ManSize(100)"`
	State     int    `form:"State" valid:"Range(0,1)"`
}

// @Summary 增文章标签
// @Tags   Tags 标签信息
// @Accept json
// @Produce  json
// @Param token  header  string  true  "Header Token"
// @Param name query string true "标签名称"
// @Param created_by query string true "创建人"
// @Param state query int true "状态=>0，1"
// @Success 200 {object} models.Response
// @Router /api/v1/tag [post]
func AddTag(c *gin.Context) {

	var (
		appG     = app.Gin{C: c}
		formData *AddTagForm
	)
	httpCode, errCode := app.BindAndValid(c, &formData)

	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, nil)
		return

	}

	tagService := &tag_service.Tag{
		Name:      formData.Name,
		CreatedBy: formData.CreatedBy,
		State:     formData.State,
	}

	exist, err := tagService.ExistByName()
	if err != nil {
		appG.Response(http.StatusInternalServerError, e.ERROR_GET_TAGS_FAIL, nil)
		return

	}

	if exist {
		appG.Response(http.StatusOK, e.ERROR_EXIST_TAG, nil)
		return
	}

	err = tagService.Add()
	if err != nil {
		appG.Response(http.StatusInternalServerError, e.ERROR_ADD_TAG_FAIL, nil)
	}

	appG.Response(http.StatusOK, e.SUCCESS, nil)

}

type EditTagForm struct {
	ID         string `form:"id" valid:"Required"`
	Name       string `form:"name" valid:"Required;ManSize(100)"`
	ModifiedBy string `form:"modified_by" valid:"Required;ManSize(100)"`
	State      int    `form:"State" valid:"Range(0,1)"`
}

// @Summary 修改章标签
// @Tags   Tags 标签信息
// @Accept json
// @Produce  json
// @Param token  header  string  true  "Header Token"
// @Param id path string true "被编辑标签key"
// @Param name query string true "标签名称"
// @Param modified_by query string true "编辑人"
// @Param state query int false "状态=>0，1"
// @Success 200 {object} models.Response
// @Router /api/v1/tag/{id} [put]
func EditTag(c *gin.Context) {

	var (
		appG     = app.Gin{C: c}
		formData = &EditTagForm{ID: c.Param("id")}
	)

	httpCode, errCode := app.BindAndValid(c, &formData)

	if errCode != e.SUCCESS {
		appG.Response(httpCode, errCode, nil)
		return

	}

	tagService := &tag_service.Tag{
		ID:         formData.ID,
		Name:       formData.Name,
		ModifiedBy: formData.ModifiedBy,
		State:      formData.State,
	}

	exist, err := tagService.ExistByID()
	if err != nil {
		appG.Response(http.StatusInternalServerError, e.ERROR_GET_TAGS_FAIL, nil)
		return
	}

	if !exist {
		appG.Response(http.StatusBadRequest, e.ERROR_EXIST_TAG_FAIL, nil)
		return
	}

	err = tagService.Edit()
	if err != nil {
		appG.Response(http.StatusInternalServerError, e.ERROR_EDIT_TAG_FAIL, nil)
	}

	appG.Response(http.StatusOK, e.SUCCESS, nil)

}

// @Summary 删除文章标签
// @Tags   Tags 标签信息
// @Accept json
// @Produce  json
// @Param token  header  string  true  "Header Token"
// @Param id path string true "被删除标签key"
// @Success 200 {object} models.Response
// @Router /api/v1/tag/{id} [delete]
func DeleteTag(c *gin.Context) {

	appG := &app.Gin{C: c}
	valid := validation.Validation{}
	ID := c.Param("id")
	valid.Required(ID, "id").Message("ID不能为空")

	if valid.HasErrors() {
		app.MarkErrors(valid.Errors)
		appG.Response(http.StatusBadRequest, e.INVALID_PARAMS, nil)
		return

	}

	tagService := &tag_service.Tag{ID: ID}

	exist, err := tagService.ExistByID()
	if err != nil {
		appG.Response(http.StatusInternalServerError, e.ERROR_GET_TAGS_FAIL, nil)
		return
	}

	if !exist {
		appG.Response(http.StatusBadRequest, e.ERROR_NOT_EXIST_TAG, nil)
		return
	}

	err = tagService.Delete()
	if err != nil {
		appG.Response(http.StatusInternalServerError, e.ERROR_DELETE_TAG_FAIL, nil)
	}

	appG.Response(http.StatusOK, e.SUCCESS, nil)

}
