package v1

import (
	"go-gin-api/models"
	"go-gin-api/pkg/e"
	"go-gin-api/pkg/logging"
	"go-gin-api/pkg/setting"
	"go-gin-api/pkg/util"
	"net/http"

	"github.com/astaxie/beego/validation"
	"github.com/gin-gonic/gin"
	"github.com/unknwon/com"
)

// @Summary 获取单个文章信息
// @Tags   Article 文章信息
// @Accept json
// @Produce  json
// @Param token  header  string  true  "Header Token"
// @Param id path string true "文章key"
// @Success 200 {object} models.Article
// @Router /api/v1/article/{id} [get]
func GetArticle(c *gin.Context) {
	id := com.StrTo(c.Param("id")).MustInt()

	valid := validation.Validation{}
	valid.Min(id, 1, "id").Message("id必须大于0")

	code := e.INVALID_PARAMS
	msg := e.GetMsg(code)
	var data interface{}
	if !valid.HasErrors() {
		if exist, _ := models.ExistArticleByID(id); exist {
			data, _ = models.GetArticle(id)
			code = e.SUCCESS
		} else {
			code = e.ERROR_NOT_EXIST_ARTICLE
		}
		msg = e.GetMsg(code)

	} else {
		for _, err := range valid.Errors {
			msg = err.Message
		}
	}

	if code != e.SUCCESS {
		logging.Error(msg)
	}

	c.JSON(http.StatusOK, gin.H{
		"code": code,
		"msg":  msg,
		"data": data,
	})

}

// @Summary 获取多个文章信息
// @Tags   Article 文章信息
// @Accept json
// @Produce  json
// @Param token  header  string  true  "Header Token"
// @Param tag_id query string fasle "文章标签key"
// @Param state query string fasle "状态0，1"
// @Success 200 {object} models.Article
// @Router /api/v1/articles [get]
func GetArticles(c *gin.Context) {
	data := make(map[string]interface{})
	maps := make(map[string]interface{})
	valid := validation.Validation{}

	var state int = -1
	if arg := c.Query("state"); arg != "" {
		state = com.StrTo(arg).MustInt()
		maps["state"] = state

		valid.Range(state, 0, 1, "state").Message("状态只允许0或1")
	}

	var tagId int = -1
	if arg := c.Query("tag_id"); arg != "" {
		tagId = com.StrTo(arg).MustInt()
		maps["tag_id"] = tagId

		valid.Min(tagId, 1, "tag_id").Message("标签ID必须大于0")

	}

	code := e.INVALID_PARAMS
	msg := e.GetMsg(code)
	if !valid.HasErrors() {
		code = e.SUCCESS
		data["lists"], _ = models.GetArticles(util.GetPage(c), setting.AppSetting.PageSize, maps)
		data["total"], _ = models.GetArticleTotal(maps)
		msg = e.GetMsg(code)
	} else {
		for _, err := range valid.Errors {
			msg = err.Message
			logging.Info(msg)
		}

	}

	if code != e.SUCCESS {
		logging.Error(msg)
	}
	c.JSON(http.StatusOK, gin.H{
		"code": code,
		"msg":  msg,
		"data": data,
	})

}

// @Summary 新增文章标签
// @Tags   Article 文章信息
// @Accept json
// @Produce  json
// @Param token  header  string  true  "Header Token"
// @Param tag_id query string true "文章标签key"
// @Param title query string true "文章标题"
// @Param desc query string true "文章表述"
// @Param content query string true "文章内容"
// @Param created_by query string true "创建人"
// @Param state query int true "状态=>0，1"
// @Success 200 {object} models.Response
// @Router /api/v1/article [post]
func AddArticle(c *gin.Context) {

	tagId := com.StrTo(c.Query("tag_id")).MustInt()
	title := c.Query("title")
	desc := c.Query("desc")
	content := c.Query("content")
	createdBy := c.Query("created_by")
	state := com.StrTo(c.DefaultQuery("state", "0")).MustInt()

	valid := validation.Validation{}
	valid.Min(tagId, 1, "tag_id").Message("标签ID必须大于0")
	valid.Required(title, "title").Message("标题不能为空")
	valid.Required(desc, "desc").Message("描述不能为空")
	valid.Required(content, "content").Message("内容不能为空")
	valid.Required(createdBy, "created_by").Message("创建人不能为空")
	valid.Range(state, 0, 1, "state").Message("状态只允许0或1")

	code := e.INVALID_PARAMS
	msg := e.GetMsg(code)
	if !valid.HasErrors() {
		if exist, _ := models.ExistTagByID(tagId); !exist {
			data := make(map[string]interface{})
			data["tag_id"] = tagId
			data["title"] = title
			data["desc"] = title
			data["content"] = content
			data["created_by"] = createdBy
			data["state"] = state

			models.AddArticle(data)
			code = e.SUCCESS

		} else {
			code = e.ERROR_NOT_EXIST_TAG
		}
		msg = e.GetMsg(code)

	} else {

		for _, err := range valid.Errors {
			msg = err.Message
		}

	}

	c.JSON(http.StatusOK, gin.H{
		"code": code,
		"msg":  msg,
		"data": make(map[string]interface{}),
	})

}

// @Summary 编辑文章标签
// @Tags   Article 文章信息
// @Accept json
// @Produce  json
// @Param token  header  string  true  "Header Token"
// @Param id path string true "被编辑文章key"
// @Param tag_id query string false "文章标签id"
// @Param title query string false "文章标题"
// @Param desc query string false "文章表述"
// @Param content query string false "文章内容"
// @Param modified_by query string true "编辑人"
// @Param state query int false "状态=>0，1"
// @Success 200 {object} models.Response
// @Router /api/v1/article/{id} [put]
func EditArticle(c *gin.Context) {
	valid := validation.Validation{}

	id := com.StrTo(c.Param("id")).MustInt()
	tagId := com.StrTo(c.Query("tag_id")).MustInt()
	title := c.Query("title")
	desc := c.Query("desc")
	content := c.Query("content")
	modifiedBy := c.Query("modified_by")

	var state int = -1
	if arg := c.Query("state"); arg != "" {

		state = com.StrTo(arg).MustInt()
		valid.Range(state, 0, 1, "state").Message("状态只允许0或1")
	}

	valid.Min(id, 1, "id").Message("ID必须大于0")
	valid.MaxSize(title, 100, "title").Message("标题最长为100字符")
	valid.MaxSize(desc, 255, "desc").Message("简述最长为255字符")
	valid.MaxSize(content, 65535, "content").Message("内容最长为65535字符")
	valid.Required(modifiedBy, "modified_by").Message("修改人不能为空")
	valid.MaxSize(modifiedBy, 100, "modified_by").Message("修改人最长为100字符")

	code := e.INVALID_PARAMS
	msg := e.GetMsg(code)
	if !valid.HasErrors() {
		if exist_article, _ := models.ExistArticleByID(id); exist_article {
			if exist_tag, _ := models.ExistTagByID(tagId); exist_tag {
				data := make(map[string]interface{})
				if tagId > 0 {
					data["tag_id"] = tagId
				}
				if title != "" {
					data["title"] = title
				}
				if desc != "" {
					data["desc"] = desc
				}
				if content != "" {
					data["content"] = content
				}

				data["modified_by"] = modifiedBy

				models.EditAtricle(id, data)
				code = e.SUCCESS
			} else {
				code = e.ERROR_NOT_EXIST_TAG
			}
		} else {
			code = e.ERROR_NOT_EXIST_ARTICLE
		}

		msg = e.GetMsg(code)

	} else {
		for _, err := range valid.Errors {
			msg = err.Message
		}
	}

	c.JSON(http.StatusOK, gin.H{
		"code": code,
		"msg":  msg,
		"data": make(map[string]interface{}),
	})

}

// @Summary 删除文章
// @Tags   Article 文章信息
// @Accept json
// @Produce  json
// @Param token  header  string  true  "Header Token"
// @Param id path string true "被删除文章key"
// @Success 200 {object} models.Response
// @Router /api/v1/article/{id} [delete]
func DeleteArticle(c *gin.Context) {
	id := com.StrTo(c.Param("id")).MustInt()
	valid := validation.Validation{}

	valid.Min(id, 1, "id").Message("文件ID必须大于0")

	code := e.INVALID_PARAMS
	msg := e.GetMsg(code)
	if !valid.HasErrors() {
		if exist, _ := models.ExistArticleByID(id); exist {
			models.DeleteArticle(id)
			code = e.SUCCESS
		} else {
			code = e.ERROR_NOT_EXIST_ARTICLE
		}
		msg = e.GetMsg(code)
	} else {
		for _, err := range valid.Errors {
			msg = err.Message
			//log.Printf("err.key: %s, err.message: %s", err.Key, err.Message)
		}
	}

	c.JSON(http.StatusOK, gin.H{
		"code": code,
		"msg":  msg,
		"data": make(map[string]interface{}),
	})

}
