package routers

import (
	"go-gin-api/docs"
	"go-gin-api/pkg/setting"
	"go-gin-api/pkg/upload"
	v1 "go-gin-api/routers/api/v2"
	"net/http"

	"go-gin-api/routers/api"

	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
)

func InitRouter() *gin.Engine {

	docs.SwaggerInfo.Title = "go-gin-api Swagger API 接口文档示列"
	docs.SwaggerInfo.Description = "go-gin-api 接口描述内容"
	docs.SwaggerInfo.Version = "2.0"
	// docs.SwaggerInfo.Host = "htttp://localhost:8100"
	// docs.SwaggerInfo.BasePath = "/swagger/index.html"
	docs.SwaggerInfo.Schemes = []string{"http", "https"}

	//返回Gin 的type Engine struct{}结构体，里面包含了RouterGroup，相当于创建一个路由Handlers,可以后期绑定各类的路由规则和函数中间件
	//router := gin.Default()

	r := gin.New()
	r.Use(gin.Logger())
	r.Use(gin.Recovery())
	gin.SetMode(setting.ServerSeting.RunMode)

	//1.创建不同类型的http方法绑定到Handler中，Get Post Put Delete Header Patch Options
	//2.其中 gin.h{...}其实就是一个map 空接口 map[sting]interface{}
	//3.gin.Context：Context是gin中的上下文，它允许我们在中间件之间传递变量、管理流、验证 JSON 请求、响应 JSON 请求等，在gin中包含大量Context的方法，例如我们常用的DefaultQuery、Query、DefaultPostForm、PostForm等等
	// r.GET("/test", func(c *gin.Context) {
	// 	c.JSON(200, gin.H{
	// 		"message": "test",
	// 	})
	// })
	r.StaticFS("/upload/images", http.Dir(upload.GetImageFullPath()))

	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
	r.GET("/auth", api.GetAuth)              //获取权限 jwt token
	r.POST("/upload/image", api.UploadImage) //图片上传

	apiv1 := r.Group("/api/v2")
	//apiv1.Use(jwt.JWT())
	{

		//标签管理  API
		apiv1.GET("/tags", v1.GetTags)         //获取集合
		apiv1.POST("/tag", v1.AddTag)          //新增对象
		apiv1.PUT("/tag/:id", v1.EditTag)      //便捷对象
		apiv1.DELETE("/tag/:id", v1.DeleteTag) //删除对象

		//文章管理  API
		apiv1.GET("/article/:id", v1.GetArticle)       //获取集合
		apiv1.GET("/articles", v1.GetArticles)         //获取集合
		apiv1.POST("/article", v1.AddArticle)          //新增对象
		apiv1.PUT("/article/:id", v1.EditArticle)      //便捷对象
		apiv1.DELETE("/article/:id", v1.DeleteArticle) //删除对象

	}

	return r
}
