package api

import (
	"go-gin-api/models"
	"go-gin-api/pkg/e"
	"go-gin-api/pkg/logging"
	"go-gin-api/pkg/util"
	"net/http"

	"github.com/astaxie/beego/validation"
	"github.com/gin-gonic/gin"
)

type auth struct {
	Username string `vaild:"Required;MaxSize(50)"`
	Password string `vaild:"Required;MaxSize(50)"`
}

// @Summary jwt token 获取权限信息
// @Tags   Token验证
// @Accept json
// @Produce  json
// @Param username query string true "账号"
// @Param password query string true "密码"
// @Success 200 {object} models.Response
// @Router /auth [get]
func GetAuth(c *gin.Context) {
	username := c.Query("username")
	password := c.Query("password")

	valid := validation.Validation{}
	a := auth{Username: username, Password: password}
	ok, _ := valid.Valid(&a)

	data := make(map[string]interface{})
	code := e.INVALID_PARAMS
	msg := e.GetMsg(code)
	if ok {
		isExist := models.CheckAuth(username, password)
		if isExist {
			token, err := util.GenerateToken(username, password)

			if err != nil {

				code = e.ERROR_AUTH_TOKEN
			} else {
				data["token"] = token
				code = e.SUCCESS
			}
		} else {
			code = e.ERROR_AUTH

		}
		msg = e.GetMsg(code)
	} else {
		for _, err := range valid.Errors {
			msg = err.Message
		}
	}

	if code != e.SUCCESS {
		logging.Error(msg)
	}

	c.JSON(http.StatusOK, gin.H{
		"code": code,
		"msg":  msg,
		"data": data,
	})

}
