FROM golang:latest

ENV GOPROXY https://goproxy.cn,direct
WORKDIR $GOPATH/src/goproject/go-gin-api
COPY . $GOPATH/src/goproject/go-gin-api
RUN go build .

EXPOSE 8000
ENTRYPOINT ["./go-gin-api"]