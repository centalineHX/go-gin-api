package v1

import (
	"go-gin-api/models"
	"go-gin-api/pkg/e"
	"go-gin-api/pkg/setting"
	"go-gin-api/pkg/util"
	"net/http"

	"github.com/astaxie/beego/validation"
	"github.com/gin-gonic/gin"
	"github.com/unknwon/com"
)

// @Summary 获取多个文章标签
// @Tags   Tags 标签信息
// @Accept json
// @Produce  json
// @Param token  header  string  true  "Header Token"
// @Param name query string false "标签名称"
// @Param state query int false "状态=>0，1"
// @Success 200 {object} models.Tag
// @Router /api/v1/tags [get]
func GetTags(c *gin.Context) {

	maps := make(map[string]interface{})
	data := make(map[string]interface{})

	name := c.Query("name")

	var state int = -1
	if arg := c.Query("state"); arg != "" {
		state = com.StrTo(arg).MustInt()
		maps["state"] = state
	}

	if name != "" {
		maps["name"] = name
	}

	code := e.SUCCESS

	if _filter, ok := models.AddDeletedTagField(maps); ok {

		data["lists"], _ = models.GetTags(util.GetPage(c), setting.AppSetting.PageSize, _filter)
		data["total"], _ = models.GetTagTotal(_filter)
	}

	c.JSON(http.StatusOK, gin.H{
		"code": code,
		"msg":  e.GetMsg(code),
		"data": make(map[string]string),
	})

}

// @Summary 增文章标签
// @Tags   Tags 标签信息
// @Accept json
// @Produce  json
// @Param token  header  string  true  "Header Token"
// @Param name query string true "标签名称"
// @Param created_by query string true "创建人"
// @Param state query int true "状态=>0，1"
// @Success 200 {object} models.Response
// @Router /api/v1/tag [post]
func AddTag(c *gin.Context) {

	name := c.Query("name")
	state := com.StrTo(c.DefaultQuery("state", "0")).MustInt()
	createdBy := c.Query("created_by")

	vaild := validation.Validation{}
	vaild.Required(name, "name").Message("名称不能为空！")
	vaild.MaxSize(name, 100, "name").Message("名称长度最长超过100字符")
	vaild.Required(createdBy, "created_by").Message("创建人不能为空！")
	vaild.MaxSize(createdBy, 100, "created_by").Message("创建人长度不能超过100")
	vaild.Range(state, 0, 1, "state").Message("状态只允许0或者1")

	code := e.INVALID_PARAMS
	msg := e.GetMsg(code)
	if !vaild.HasErrors() {
		if exist, _ := models.ExistTagByName(name); !exist {
			code = e.SUCCESS
			models.AddTag(name, state, createdBy)
		} else {
			code = e.ERROR_EXIST_TAG

		}
		msg = e.GetMsg(code)
	} else {

		for _, err := range vaild.Errors {
			msg = err.Message
			break
		}

	}

	c.JSON(http.StatusOK, gin.H{
		"code": code,
		"msg":  msg,
		"data": make(map[string]string),
	})

}

// @Summary 修改章标签
// @Tags   Tags 标签信息
// @Accept json
// @Produce  json
// @Param token  header  string  true  "Header Token"
// @Param id path string true "被编辑标签key"
// @Param name query string true "标签名称"
// @Param modified_by query string true "编辑人"
// @Param state query int false "状态=>0，1"
// @Success 200 {object} models.Response
// @Router /api/v1/tag/{id} [put]
func EditTag(c *gin.Context) {

	id := c.Param("id")

	name := c.Query("name")
	modifiedBy := c.Query("modified_by")

	vaild := validation.Validation{}

	var state int = -1
	if arg := c.Query("state"); arg != "" {
		state = com.StrTo(arg).MustInt()
		vaild.Range(state, 0, 1, "state").Message("状态只允许0或1")
	}

	vaild.Required(id, "id").Message("ID不能为空")
	vaild.MaxSize(name, 100, "name").Message("名称长度最长超过100字符")
	vaild.Required(modifiedBy, "modified_by").Message("修改人不能为空")
	vaild.MaxSize(modifiedBy, 100, "modified_by").Message("修改人最长不能超过100")

	code := e.INVALID_PARAMS
	msg := e.GetMsg(code)
	if !vaild.HasErrors() {
		if exist, _ := models.ExistTagByID(id); exist {
			data := make(map[string]interface{})
			data["modified_by"] = modifiedBy
			if name != "" {
				data["name"] = name
			}
			if state != -1 {
				data["state"] = state
			}

			models.EditTag(id, data)
			code = e.SUCCESS

		} else {
			code = e.ERROR_NOT_EXIST_TAG
		}
		msg = e.GetMsg(code)

	} else {

		for _, err := range vaild.Errors {
			msg = err.Message
			break
		}

	}

	c.JSON(http.StatusOK, gin.H{
		"code": code,
		"msg":  msg,
		"data": make(map[string]string),
	})

}

// @Summary 删除文章标签
// @Tags   Tags 标签信息
// @Accept json
// @Produce  json
// @Param token  header  string  true  "Header Token"
// @Param id path string true "被删除标签key"
// @Success 200 {object} models.Response
// @Router /api/v1/tag/{id} [delete]
func DeleteTag(c *gin.Context) {

	vaild := validation.Validation{}
	id := c.Param("id")
	vaild.Required(id, "id").Message("ID不能为空")

	code := e.INVALID_PARAMS
	if !vaild.HasErrors() {
		code = e.SUCCESS
		if exist, _ := models.ExistTagByID(id); exist {
			models.DeleteTag(id)
		} else {
			code = e.ERROR_NOT_EXIST_TAG

		}

	}

	c.JSON(http.StatusOK, gin.H{
		"code": code,
		"msg":  e.GetMsg(code),
		"data": make(map[string]string),
	})

}
