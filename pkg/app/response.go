package app

import (
	"go-gin-api/pkg/e"

	"github.com/gin-gonic/gin"
)

type ResponseModel struct {
	Code int         `json:"code"`
	Msg  string      `json:"message"`
	Data interface{} `json:"data"`
}

type Gin struct {
	C *gin.Context
}

// c.JSON(http.StatusOK, gin.H{
// 	"code": code,
// 	"msg":  msg,
// 	"data": data,
// })

func (g *Gin) Response(httpCode, errCode int, data interface{}) {

	g.C.JSON(httpCode, &ResponseModel{
		Code: errCode,
		Msg:  e.GetMsg(errCode),
		Data: data,
	})

}
